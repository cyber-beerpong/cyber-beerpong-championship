import sys
import time
from tkinter import *

from random import seed
import random

seed(42)

players = [
    "Bazooka",
    "THC #13",
    "Gargamel",
    "Export Import 31",
    "Hong Pong",
    "Bell Pong",
    "My little Pongy",
    "King Julien",
    "Ajax Dauerstramm",
    "Futuredocs",
    "Andi Kolben",
    "Bierkasparle",
    "Standik Besoffnson",
    "2 Balls in One Cup",
    "Pong Voyage",
    "ProSlammer",
    "Einarmige Banditin",
    "Beercules",
    "Kobeer Bryant",
    "BeerBeer Blocksberg",
    "L.K. Bierdurst Marie",
    "Kühles Blondes",
    "Die Gummibierchenbande",
	"Snakebite",
]

groups = dict({
    'A': 0,
    'B': 0,
    'C': 0,
    'D': 0
})

player_random_pool = players.copy()

def get_random_player_from_pool():
    player = random.choice(player_random_pool)
    return player

def remove_player_from_pool(player):
    player_random_pool.remove(player)

def get_group(max_players):
    for group in groups:
        if groups[group] < max_players:
            return group

def add_group(group):
    groups[group] += 1

class Fullscreen_Window:

    def __init__(self):
        self.tk = Tk()
        self.tk.attributes('-zoomed', True)  # This just maximizes it so we can see the window. It's nothing to do with fullscreen.
        self.frame = Frame(self.tk)

        self.NUM_COLS = 4
        self.NUM_ROWS = int(len(players)/self.NUM_COLS)
        self.MAX_PER_GROUP = self.NUM_ROWS

        for i in range(self.NUM_COLS):
            self.frame.columnconfigure(i, pad=40)

        for i in range(0, self.NUM_ROWS+2):
            self.frame.rowconfigure(i, pad=80)

        header_label = Label(self.frame, text="1st Cyber Beerpong Championship", font=("Courier", 44),)
        header_label.grid(column=0, row=0, columnspan=self.NUM_COLS, ipadx=20, ipady=20, sticky="NSEW")

        self.player_labels = dict({})
        count = 0
        for player in players:
            self.player_labels[player] = Label(
                self.frame,
                text=player,
                bg = "white",
                font=("Courier", 25),
            )

            self.player_labels[player].grid(row=int(count/self.NUM_COLS) + 1, column=count % self.NUM_COLS)
            count += 1

        #self.button = Button(self.frame,
        #                     text="Random",
        #                     command=self.start_randomizer)

        self.group = Label(
                self.frame,
                text="Group: {}".format(get_group(self.MAX_PER_GROUP)),
                bg = "light grey",
                font=("Courier", 36),
            )
        self.group.grid(row=self.NUM_ROWS + 1, column=0)

        self.frame.pack()

        self.random_active = False

    def start_randomizer(self, _):

        if self.random_active:
            print("Randomizer already running!")
            return

        self.random_active = True

        print("Randomizer started")
        player = None
        for _ in range(40):
            player = get_random_player_from_pool()
            print(player)
            self.tk.after(100, self.flash_label(player, "dark green"))
            self.tk.after(100, self.flash_label(player, "white"))

        new_text = "{} ({})".format(self.player_labels[player].cget("text"), get_group(self.MAX_PER_GROUP))
        self.player_labels[player].configure(text=new_text, bg = "red")
        remove_player_from_pool(player)
        add_group(get_group(self.MAX_PER_GROUP))
        self.group.configure(text="Group: {}".format(get_group(self.MAX_PER_GROUP)))

        self.random_active = False


    def flash_label(self, player, color):
        self.player_labels[player].configure(background=color)
        self.tk.update()


if __name__ == '__main__':
    w = Fullscreen_Window()
    w.tk.title("Auslosung")
    w.tk.bind('<space>', w.start_randomizer)
    w.tk.mainloop()

---
title: 'Spielregeln Beerpong Championship v1.0'
author: 'Bazooka (Cyber Beerpong Commissioner)'
date: '03. Februar 2021'
numbersections: true
geometry: margin=2cm
---

# Aufbau / Equipment

a. Becher werden in 6er Kombination im Waeschestaender aufgebaut und mit Wasser befuellt.
b. Die Becher Nummerierung ist

```
                    4 5 6
                     2 3
                      1
```

c. 3 Glaeser fuer Trinkfuellung
d. Wurfdistanz zu Becher 5 sollte mindestens 160 cm betragen.
e. Mindestens 2 Tischtennis Baelle. *Hinweis*: Genuegend Ersatzbaelle bereitlegen!
f. Material um aus dem Spiel genommene Becher zu markieren (z.B. Kronkorken).

# Reglen & Ablauf

## Grundlegende Spielregeln

a. Der erstgenanne Spieler einer Partie beginnt.
b. Keine Ausgleichwuerfe des zweitgenannten Spielers.
c. Pro Wurfrunde 2 Wuerfe.
d. Ellenbogen bei Wurf hinter Waeschestaenderkante (Fairplay! ;-))
e. Getroffener Becher ist sofort aus dem Spiel (auch beim Einchecken) wird aber **nicht** vom
   Waeschestaender genommen. Daher wird bei Doppeltreffer in einen Becher der zweite Wurf als
   Fehlwurf gewertet.
f. Im Gegensatz zum normalen Bierpong gibt es kein Becherumstellen / Rekombinieren. Gekippte
   Becher duerfen vor Beginn der Wurfrunde sauber aufgestellt werden.
g. Abpraller von Becher, egal ob der Becher noch im Spiel ist oder nicht, sind gueltig.
h. Abpraller vom Waeschestaender sind ebenfalls gueltig.
i. Abpraller von allen anderen Gegenstaenden sind nicht gueltig.
  
## Spielablauf
  
a. Pro Spiel werden 2 Saetze gespielt (in der KO Phase gibt es bei Gleichstand einen
   Entscheidungssatz). Der erstgenanne Spieler beginnt.
b. Die Spiele sind zeitlich begrenzt (20 min). Kann der zweite Satz nicht begonnen werden,
   geht der Satz als 0:0 in die Wertung ein.

### Besonderheit: Ein- und Auschecken

a. Das Spiel ist eroeffnet durch einen Treffer in Becher 5.
b. Das Spiel wird beendet durch einen Treffer in Becher 1.
c. Wird Becher 1 oder 5 nicht beim Ein- bzw. Auschecken getroffen, wird der Wurf als Fehlwurf
   gewertet.
d. Die uebrigen Becher duerfen dann in beliebiger Reihenfolge ausgeworfen werden.

## Becher Befuellung und Leerung

a. Erlaubte Becherfuellung (Bier, Radler, alkoholfrei)
b. Pro Satz werden 100 ml in 3 Glaeser geschenkt.
c. Pro Treffer halbes Glas trinken.
d. Nach Ende des Satzes angefangenes Glas leertrinken.

# Turnier Ablauf

## Allgemeines

a. Es nehmen 24 Spieler teil welche sich auf 4 Gruppen je 6 Spieler aufteilen.
b. Die besten 4 Spieler je Gruppe ziehen in die KO Phase ein.
c. Die uebrigen Teams spielen ebenfalls in einer KO Phase die finale Platzierung aus.

## Gruppenphase

a. Pro gewonnenem Satz wird ein Punkt verteilt, so koennen pro Spiel 2, 1 oder 0 Punkte erzielt werden.
b. Die getroffenen und kassierten Becher gehen ebenfalls in die Wertung ein.
c. Bei Punktegleichstand entscheiden folgende Kriterien in der Reihenfolge:
    - Direkter Vergleich
    - Becherdifferenz aus getroffene Becher und kassierte Becher
    - Staerke des alkoholischen Getraenks (Bier schlaegt Radler schlaegt alkoholfrei)
    - Auslosung durch Commissioner

## KO Phase

a. Der Spieler mit der besseren Platzierung aus der Gruppenphase erhaelt Anwurfrecht im Entscheidungssatz.
b. Haben beide Spieler die gleiche Platzierung wird Anwurfrecht gilt in folgender Reihenfolge:
     - Staerke des alkoholischen Getraenks (Bier schlaegt Radler schlaegt alkoholfrei)
     - Auslosung durch Commissioner
c. Spiele in KO Runde werden ebenfalls zeitlich begrenzt mit Ausname des Finales / der Platzierungsspiele.

# Technik

## Technischer Ablauf

a. Organisiations Plattform ist Telegram.
b. Videokonferenz Plattform ist Jitsi.
c. Videokonferenz Raeume oeffnen 5 Minuten vor Anwurf und werden im Telegram Channel bekanntgegeben.
d. Der erstgenannte Spieler meldet Ergebnisse im Telegram Channel unter der entsprechenden
   Nachricht mit den Chatraeumen der Spielrunde. Gemeldet werden die getroffenen Becher pro Satz, z.B.
   6:3, 4:6 oder z.B. bei abgebrochener Spielrunde 2:5 0:0.

## Bei technischen Problemen

Sollte es zu technischen Problem kommen, z.B. Verlust Internetverbindung das Spiel via Telefon beenden.
Im Zweifel Commissionar Bazooka telefonisch kontaktieren zur Problemfindung. :-)
